package main

import (
	"io"
	"log"
	"net"
	"os"
	"time"

	"gitee.com/ziegate/melsec"
)

func udp() {
	log.Println("UDP Test")

	conn, err := melsec.NewUDPConn("127.0.0.1", "60000", "", "", melsec.SetCPUTimer(uint16(10)))
	if err != nil {
		log.Println(err)

		os.Exit(2)
	}
	defer func() {
		_ = conn.Close()
	}()

	svDevice1, err := melsec.NewDevice(os.Args[1], 1, conn)
	if err != nil {
		log.Println(err)

		os.Exit(3)
	}

	if err = svDevice1.Read(false); err != nil {
		log.Println(err)

		os.Exit(4)
	}

	os.Exit(0)
}

func main() {
	udp()

	if len(os.Args) != 2 {
		log.Println("invalid address")

		os.Exit(1)
	}

	conn, err := melsec.NewTCPConn("192.168.0.28", "502", nil, melsec.SetCPUTimer(uint16(10)))
	if err != nil {
		log.Println(err)

		os.Exit(2)
	}
	defer func() {
		_ = conn.Close()
	}()

	t := time.Now()

	svDevice1, err := melsec.NewDevice(os.Args[1], 1, conn)
	if err != nil {
		log.Println(err)

		os.Exit(3)
	}

	if err := svDevice1.Read(true); err != nil {
		log.Println(err)

		os.Exit(4)
	}

	log.Println(svDevice1.GetValue())

	//svDevice2, err := melsec.NewMultiDevice(conn)
	//if err != nil {
	//	log.Fatal(err)
	//}

	// 添加数据区块
	//svDevice1.AddBlock("D12000", 48) // 0
	//svDevice1.AddBlock("D12100", 12) // 1
	//svDevice1.AddBlock("D12200", 6)  // 2
	//svDevice1.AddBlock("D12300", 34) // 3
	//svDevice1.AddBlock("D12400", 24) // 4
	//svDevice1.AddBlock("D1270", 20)  // 5
	//svDevice1.AddBlock("R102", 10)   // 6
	//
	//svDevice2.AddBlock("R320", 2)    // 7
	//svDevice2.AddBlock("R450", 8)    // 8
	//svDevice2.AddBlock("M20000", 10) // 9
	//svDevice2.AddBlock("W1000", 8)   // 10
	//svDevice2.AddBlock("X800", 4)    // 11
	//svDevice2.AddBlock("D1600", 2)   // 12
	//svDevice2.AddBlock("W651B", 2)   // 13
	//svDevice2.AddBlock("D600", 2)    // 17
	//svDevice2.AddBlock("D670", 2)    // 18
	//
	//devices := []*melsec.MultiDevice{svDevice1, svDevice2}
	//
	//re := make([][]byte, 0)
	//
	//t = time.Now()
	//for i := range devices {
	//	if err := devices[i].Read(); err != nil {
	//		log.Fatal(err)
	//	}
	//
	//	re = append(re, devices[i].GetValue()...)
	//	log.Println("Multi: ", i)
	//}
	//log.Println(time.Since(t).Milliseconds(), "ms")
	//
	//svDevice, err := melsec.NewDevice("D12000", 1, conn)
	//if err != nil {
	//	log.Fatal(err)
	//}
	//
	//t = time.Now()
	//err = svDevice.Read()
	//if err != nil {
	//	log.Fatal(err)
	//}
	log.Println(time.Since(t).Milliseconds(), "ms")
}

func socket() {
	udpAddr, err := net.ResolveUDPAddr("udp", "localhost:60000")
	if err != nil {
		log.Panic(err)
	}

	conn, err := net.ListenUDP("udp", udpAddr)
	if err != nil {
		log.Panic(err)
	}

	err = conn.SetReadBuffer(1024)
	if err != nil {
		log.Panic(err)
	}

	buf := make([]byte, 1024)

	for {
		n, _, err := conn.ReadFrom(buf)
		if err != nil {
			log.Println(err)
		}

		log.Println(n)
		log.Printf("% x", buf)

		clear(buf)
	}
	return

	_, err = io.ReadFull(conn, buf)
	if err != nil {
		log.Panic(err)
	}

	log.Printf("% x", buf)

	clear(buf)

	_, err = io.ReadFull(conn, buf)
	if err != nil {
		log.Panic(err)
	}

	log.Printf("% x", buf)

	_ = conn.Close()
}
