package melsec

import (
	"bytes"
	"fmt"
	"io"
	"log"
	"net"
	"reflect"
)

const (
	FirstResponseLength     = 11 // 副帧头=2, 访问路径=7, 数据长度=2
	ResponseErrorCodeIndex  = 9
	ResponseErrorCodeLength = 2
)

func NewTCPConn(addr, port string, ops ...PlcOption) (PlcConn, error) {
	tcpAddr, err := net.ResolveTCPAddr("tcp", addr+":"+port)
	if err != nil {
		return nil, err
	}

	conn, err := net.DialTCP("tcp", nil, tcpAddr)
	if err != nil {
		return nil, err
	}

	if err = conn.SetKeepAlive(true); err != nil {
		return nil, err
	}

	return &tcpConn{
		conn,
		newPlcOption(ops),
	}, nil
}

// NewUDPConn localAddr与localPort为空字符串时, 随机指定本地端口. 设置读缓存为1024
func NewUDPConn(remoteAddr, remotePort, localAddr, localPort string, ops ...PlcOption) (PlcConn, error) {
	raddr, err := net.ResolveUDPAddr(remoteAddr, remotePort)
	if err != nil {
		return nil, err
	}

	var laddr *net.UDPAddr

	if localAddr == "" && localPort == "" {
		laddr = nil
	} else {
		laddr, err = net.ResolveUDPAddr(localAddr, localPort)
		if err != nil {
			return nil, err
		}
	}

	conn, err := net.DialUDP("udp", laddr, raddr)
	if err != nil {
		return nil, err
	}

	err = conn.SetReadBuffer(1024)
	if err != nil {
		return nil, err
	}

	return &udpConn{
		UDPConn: conn,
		option:  newPlcOption(ops),
	}, nil
}

type PlcConn interface {
	SendCmd(message McMessage, retSize int, debug bool) ([]byte, error)
	GetCPUInfo() (string, error)
	options() *plcOptions
	Close() error
}

type tcpConn struct {
	net.Conn
	option *plcOptions
}

func (plc *tcpConn) SendCmd(msg McMessage, retSize int, debug bool) ([]byte, error) {
	_, err := plc.Write(msg)
	if err != nil {
		return nil, err
	}

	buff := make([]byte, FirstResponseLength)

	_, err = io.ReadFull(plc, buff)
	if err != nil {
		return nil, fmt.Errorf("got % x, %w", buff, err)
	}

	if debug {
		log.Printf("first response: % x", buff)
	}

	// 返回错误代码
	if errorCode := buff[ResponseErrorCodeIndex : ResponseErrorCodeIndex+ResponseErrorCodeLength]; !reflect.DeepEqual(errorCode, CodeOK) {
		buff = make([]byte, ResponseErrorCodeLength)

		_, err = io.ReadFull(plc, buff)
		if err != nil {
			return nil, err
		}

		log.Printf("errorcode: % x", buff)

		return nil, ErrorSelect(errorCode)
	}

	if retSize == 0 {
		return nil, nil
	}

	buff = make([]byte, retSize)

	_, err = io.ReadFull(plc, buff)
	if err != nil {
		return nil, err
	}

	return buff, nil
}

func (plc *tcpConn) options() *plcOptions {
	return plc.option
}

func (plc *tcpConn) GetCPUInfo() (string, error) {
	cmd, err := plc.option.makeRequest(getCPUInfo())
	if err != nil {
		return "", err
	}

	_b, err := plc.SendCmd(cmd, 18, false)
	if err != nil {
		return "", err
	}

	return string(bytes.TrimSpace(_b)), nil
}

func getCPUInfo() McMessage {
	return []byte{0x01, 0x01, 0x00, 0x00}
}

type udpConn struct {
	*net.UDPConn
	option *plcOptions
}

func (plc *udpConn) SendCmd(msg McMessage, retSize int, debug bool) ([]byte, error) {
	_, err := plc.Write(msg)
	if err != nil {
		return nil, err
	}

	buff := make([]byte, 1024)
	n, _, err := plc.UDPConn.ReadFromUDP(buff)
	if err != nil {
		return nil, err
	}

	buff = buff[:n]

	if debug {
		log.Printf("response: % x", buff)
	}

	// 返回错误代码
	if errorCode := buff[ResponseErrorCodeIndex : ResponseErrorCodeIndex+ResponseErrorCodeLength]; !reflect.DeepEqual(errorCode, CodeOK) {
		return nil, ErrorSelect(errorCode)
	}

	if retSize == 0 {
		return nil, nil
	}

	buff = buff[ResponseErrorCodeIndex+ResponseErrorCodeLength:]

	return buff, nil
}

func (plc *udpConn) GetCPUInfo() (string, error) {
	cmd, err := plc.option.makeRequest(getCPUInfo())
	if err != nil {
		return "", err
	}

	_b, err := plc.SendCmd(cmd, 18, false)
	if err != nil {
		return "", err
	}

	return string(bytes.TrimSpace(_b)), nil
}

func (plc *udpConn) options() *plcOptions {
	return plc.option
}

func (plc *udpConn) Close() error {
	return nil
}
